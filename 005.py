#!/usr/bin/env python
# -*- coding: utf8 -*-

from common import *

prTitle('5: Smallest multiple of all numbers from 1 to 20')

multiples = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]

def p5_divides_to(num):
	for i in multiples:
		if (num%i!=0):
			return i
	return 20

# We will calculate multiples of 20 until we arrive at a solution
# Since 60 is the smallest multiple of 20 divisible by 1,2,3,4,5,6 we will
# start there.

product = 60
term = 6
while (term < 20):
	product += 20
	term = p5_divides_to(product)

print('20 * {:,} = {:,}'.format(product/20, product))
