#!/usr/bin/env python
# -*- coding: utf8 -*-

from common import *

prTitle ('1: Sum of multiples of 3 or 5 below 1000')

counter = 1
summate = 0
terms = list()
output = ''
limit=1000
limit-=1
while counter < limit:
	counter+=1
	if (counter%3 == 0) or (counter%5 == 0):
		terms.append(counter)
		summate+=counter

for x in terms:
	output += '{:d} + '.format(x)

print(output[:-2] + '= {:,}'.format(summate))
