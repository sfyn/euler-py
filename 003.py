#!/usr/bin/env python
# -*- coding: utf8 -*-

from common import *

target = 600851475143
factors = list()

output = ''

prTitle('3: Largest prime factor of {:,}'.format(target))

low = 2
check = False
while not (check):
	while ((target%low)!=0):
		low += 1
	high = target/low
	check = isPrime(high)
	print ('Checking factor {:,}: '.format(high))
	print (check)
	if not check:
		low += 1
