#!/usr/bin/env python
# -*- coding: utf8 -*-

from common import *

prTitle('7: 10 001st Prime')

goal = 10001
limit = goal*2
oldLim = limit

primes = findPrimes(limit)
while (len(primes) < goal):
	print("Found {:} prime numbers below {:}".format(len(primes), limit))
	oldLim = limit
	limit += (goal - len(primes))*2
	primes = findPrimes(limit, oldLim, primes)

print()
print("The {:}st prime is: {:}".format(goal, primes.pop()))
