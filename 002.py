#!/usr/bin/env python
# -*- coding: utf8 -*-

from common import *

prTitle('2: Sum of even terms in Fibonacci seq. below 4 mill.')

previous = 1
current = 2
summate = 0
terms = list()
output = ''
limit=4000000
limit-=1
while current < limit:
	if (current%2 == 0):
		terms.append(current)
		summate += current
	new = previous+current
	previous = current
	current = new

for x in terms:
	output += '{:,} + '.format(x)

print(output[:-2] + '= {:,}'.format(summate))
