#!/usr/bin/env python
# -*- coding: utf8 -*-

from common import *

prTitle('4: Highest three digit palindrome')

def p4_val_factory(a,b):
	return {'x':a, 'y':b, 'p':a*b}

def p4_print_val(vald):
	print ('{:d} * {:d} = {:,}'.format(vald['x'], vald['y'], vald['p']))

def p4_is_palindrome(num):
	forward = str(num)
	for i, c in enumerate(reversed(forward)):
		if (c!=forward[i]):
			return False
	return True

def p4_y_loop(x, check=(999*999)):
	y=999
	product = x*y
	while not p4_is_palindrome(product) and product > check:
		y -= 1
		product = x*y
	return p4_val_factory(x,y)

x=999
high = p4_y_loop(x, 1)
while (x>0):
	x -= 1
	val = p4_y_loop(x, high['p'])
	if p4_is_palindrome(val['p']) and (val['p'] > high['p']):
		high = val


p4_print_val(high)
