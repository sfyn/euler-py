#!/usr/bin/env python
# -*- coding: utf8 -*-

from common import *


top = 100
sq = 0

sumOfSqs = 0
sqOfSums = 0

prTableLine(['VALUE','SQUARE','SUM', 'SUM OF SQS'], True)
prTableLine(['','','', ''])

while (top > 0):
	sq = top*top
	sqOfSums += top
	sumOfSqs += sq
	prTableLine([top, sq, sqOfSums, sumOfSqs])
	top -= 1

sqOfSums = sqOfSums * sqOfSums

print ()
prTitle('6: Sum square difference of the first hundred natural numbers')
print ("     Sum of squares: " + str(sumOfSqs))
print ("     Square of sums: " + str(sqOfSums))
print ("     Difference between the sum of squares and the square of sums:")
print ("                     " + str(sqOfSums-sumOfSqs))

