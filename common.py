#!/usr/bin/env python
# -*- coding: utf8 -*-

import logging

# Set up logging
logger = logging.getLogger('euler_common')
logger.setLevel(logging.DEBUG)

def _conHandler(level=logging.ERROR):
	ch = logging.StreamHandler()
	ch.setLevel(level)
	ch.setFormatter(logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s'))
	return ch

logger.addHandler(_conHandler())

def fTitle(text):
	text += "\n"
	for x in range(1,len(text)):
		text += '-'
	return text

def prTitle(text):
	print(fTitle(text))

def prTableLine(vals, center=False, space=12):
	"""Print evenly spaced table lines"""
	vals.reverse()
	val = 0
	line = '| '
	cell = '{:>' + str(space) + '} | '
	if (center):
		cell = '{:^' + str(space) + '} | '
	while (len(vals) > 0):
		val = cell.format(vals.pop())
		line += val
	print(line)

def genOdds(lower=1,upper=12):
	"""Generator for odd numbers"""
	logger.debug("Generate odds between {:} and {:}".format(lower, upper))
	if lower > 1:
		lower = int(lower/2)
	upper = int(upper/2)
	vals = range(lower,upper)
	for i in vals:
		yield (i*2)+1

def hasFactor(num, vals):
	"""Does num have a factor in vals"""
	for x in vals:
		if (num%x==0):
			return True
	return False

def isPrime(num):
	"""Is the number prime"""
	if (num==2):
		return True
	if (num%2==0):
		return False
	else:
		oddballs = genOdds(1,num**0.5)
		for i in oddballs:
			if (num>i) and (num%i==0):
				return False
	return True
	

def findPrimes(limit, lower=1, primes=[2]):
	"""Find primes up to a certain limit"""
	oddballs = genOdds(lower,limit)
	for i in oddballs:
		#logger.debug("Checking if {:} is prime".format(i))
		if not hasFactor(i, primes):
			primes.append(i)
	logger.debug("Found {:} primes between {:} and {:}" . format(len(primes), lower, limit))
	return primes

def listProduct(lst):
	"""Find the product of a series of values passed in a list"""
	product = 1
	for n in lst:
		product *= n
	return product
